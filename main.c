#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <pwd.h>
#include <grp.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <limits.h>
#include <locale.h>
#include <errno.h>

void type_short(struct stat st)
{
	if (S_ISDIR(st.st_mode)) // korzystam z gotowego makra do sprawdzania typu plikow
		printf("d");
	else if (S_ISCHR(st.st_mode))
		printf("c");
	else if (S_ISSOCK(st.st_mode))
		printf("s");
	else if (S_ISLNK(st.st_mode))
		printf("l");
	else if (S_ISBLK(st.st_mode))
		printf("b");
	else if (S_ISFIFO(st.st_mode))
		printf("p");
	else if (S_ISREG(st.st_mode))
		printf("-");
}

char* type_long(struct stat st)
{
	if (S_ISDIR(st.st_mode))
		return "Katalog";
	else if (S_ISCHR(st.st_mode))
		return "Plik specjalny";
	else if (S_ISSOCK(st.st_mode))
		return "Gniazdo";
	else if (S_ISLNK(st.st_mode))
		return "Link symboliczny";
	else if (S_ISBLK(st.st_mode))
		return "Plik specjalny przypisany";
	else if (S_ISFIFO(st.st_mode))
		return "FIFO";
	else if (S_ISREG(st.st_mode))
		return "Zwykly plik";
	return "Unknown";
}

void permissions(struct stat st)
{
	printf("%c", (st.st_mode & S_IRUSR) ? 'r' : '-');
	printf("%c", (st.st_mode & S_IWUSR) ? 'w' : '-');
	printf("%c", (st.st_mode & S_IXUSR) ? 'x' : '-');
	printf("%c", (st.st_mode & S_IRGRP) ? 'r' : '-');
	printf("%c", (st.st_mode & S_IWGRP) ? 'w' : '-');
	printf("%c", (st.st_mode & S_IXGRP) ? 'x' : '-');
	printf("%c", (st.st_mode & S_IROTH) ? 'r' : '-');
	printf("%c", (st.st_mode & S_IWOTH) ? 'w' : '-');
	printf("%c ", (st.st_mode & S_IXOTH) ? 'x' : '-');
}

void user(struct stat st)
{
	struct passwd* pw;
	pw = getpwuid(st.st_uid);
	printf("%10s ", pw->pw_name);
}

void group(struct stat st)
{
	struct group* gr;
	gr = getgrgid(st.st_gid);
	printf("%10s ", gr->gr_name);
}

char* m_date_short(time_t czas)
{
	char* tmp_time = malloc(sizeof(char) * 100);
	struct tm p;
	localtime_r(&czas, &p);
	strftime(tmp_time, 100, "%d %b %H:%M", &p);
	return tmp_time;
}

char* m_date_long(time_t czas)
{
	char* tmp_time = malloc(sizeof(char) * 100);
	struct tm p;
	localtime_r(&czas, &p);
	strftime(tmp_time, 100, "%d %B %Y roku o %H:%m:%S", &p);
	return tmp_time;
}

int how_many(DIR* dirp)
{
	struct dirent* direntp;
	int i = 0;

	errno = 0;

	for (;;)
	{

		direntp = readdir(dirp);
		if (direntp == NULL)
		{
			if (errno != 0)		// readdir zwraca NULL jezeli nie ma kolejnych
				// plikow do znalezienia oraz jezeli wystapi 
				// jakis blad, w tym drugim przypadku zmienia wartosc errno
			{
				perror("Blad podczas sprawdzania ilosci plikow");
				exit(-1);
			}
			else
				break;
		}
		else
			i++;
	}

	rewinddir(dirp);
	return i;
}

int mycmp(const char* a, const char* b)
{
	const char *cp1 = a, *cp2 = b;

	for (; (toupper(*cp1)) == toupper(*cp2); cp1++, cp2++)
	{
		if (*cp1 == '\0')
			return 0;
	}
	return ((toupper(*cp1) < toupper(*cp2)) ? -1 : +1);
}

int cmp(const void* p1, const void* p2)
{
	return mycmp(*(char* const*)p1, *(char* const*)p2);
}

char** sorting(int q, DIR* dirp)
{
	char** tab = malloc(q * sizeof(char*));
	struct dirent* direntp;
	int i = 0;
	for (i = 0; i < q; i++)
	{
		errno = 0;
		direntp = readdir(dirp);
		if (errno != 0)
		{
			perror("Blad podczas sortowania plikow");
		}
		tab[i] = malloc(sizeof(direntp->d_name) + 1);
		strcpy(tab[i], direntp->d_name);
	}

	qsort(tab, q, sizeof(char*), cmp);		// sortuje wyniki

	return tab;
}

char* linktrack(char* name, struct stat st, char tryb)
{
	// tryb: p(parametr), b(bez parametru)

	if (S_ISLNK(st.st_mode))
	{
		int roz;
		char* buffor = malloc(sizeof(name));

		roz = readlink(name, buffor, sizeof(buffor));
		buffor[roz] = '\0'; 	// niestety readlink wstawial jakis smiec
		// z pamieci wiec musialem zamknac buffor recznie
		if (tryb == 'b')
		{
			printf(" -> %s\n", buffor);

			free(buffor);
			return 0;
		}
		else if (tryb == 'p')
		{
			return buffor;
		}
	}
	else
	{
		printf("\n");
		return 0;
	}
	return 0;
}

char* path(char* file_name)
{
	char* tab = malloc(PATH_MAX);

	if (realpath(file_name, tab) == NULL)
	{
		perror("Blad odczytu sciezki");
		exit(-1);
	}
	else
	{
		return tab;
	}
}

int bez_parametru()
{
	DIR* dirp;
	dirp = opendir(".");
	if (dirp == NULL)
	{
		perror("Nie udalo sie otworzyc katalogu");
		exit(-1);
	}

	struct stat st;
	int q = how_many(dirp);
	char** tab = sorting(q, dirp);
	int i = 0;

	for (i = 0; i < q; i++)
	{
		lstat(tab[i], &st);

		type_short(st);

		permissions(st);

		printf("%2d ", (int)st.st_nlink);

		user(st);

		group(st);

		printf("%10ld ", (long int)st.st_size);

		char* tmp_time = m_date_short(st.st_mtime);
		printf("%s ", tmp_time);
		free(tmp_time);

		printf("%s", tab[i]);

		linktrack(tab[i], st, 'b');
	}

	for (i = 0; i < q; i++)
	{
		free(tab[i]);
	}
	free(tab);

	closedir(dirp);
	return 0;
}

void parametr(char* file_name)
{
	struct stat st;


	if (lstat(file_name, &st) == -1)
	{
		perror("Blad podczas sprawdzania szczegolow pliku");
		exit(-1);
	}

	printf("Informacje o %s:\n", file_name);

	printf("Typ pliku:\t%s\n", type_long(st));

	printf("Sciezka:\t%s/%s\n", path("."), file_name); 		// aby wyswietlic sciezke do linku wyswietlam
	// sciezke do katalogu nadrzednego a potem nazwe linku

	if (S_ISLNK(st.st_mode))
		printf("Wskazuje na:\t%s\n", path(linktrack(file_name, st, 'p')));

	long int roz = (long int) st.st_size;
	printf("Rozmiar:\t%ld ", roz);
	if (roz == 1)
		printf("bajt\n");
	else
		if ((10 < roz && roz < 22) || roz % 10 > 4 || roz % 10 < 2)
			printf("bajtow\n");
		else
			printf("bajty\n");

	printf("Uprawnienia:\t");
	permissions(st);
	printf("\n");

	char* tmp_time = m_date_long(st.st_atime);
	printf("Ostatnio uzywany\t%s\n", tmp_time);
	free(tmp_time);

	tmp_time = m_date_long(st.st_mtime);
	printf("Ostatnio modyfikowany\t%s\n", tmp_time);
	free(tmp_time);

	tmp_time = m_date_long(st.st_ctime);
	printf("Ostatnio zmieniany stan\t%s\n", tmp_time);
	free(tmp_time);

	if ((S_ISREG(st.st_mode)) && !(st.st_mode & S_IXUSR))
	{
		printf("Poczatek zawartosci:\n");
		char tmp;
		int i = 0;
		FILE* file = fopen(file_name, "r");
		if (file == NULL)
		{
			perror("Blad podczas wyswietlania poczatku pliku");
			exit(-1);
		}

		while (((tmp = (fgetc(file))) != EOF) &&  i < 80)
		{
			printf("%c", tmp);
			i++;
		}
		printf("\n");
	}
}

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "pl_PL.UTF-8");

	switch (argc)
	{
		case 1:
			bez_parametru();
			break;
		case 2:
			parametr(argv[1]);
			break;
		default:
			fprintf(stderr, "Podaj najwyzej 1 parametr!\n");
			break;
	}

	return 0;
}
